<?php

/**
 * @file
 * Default rule configurations for Commerce Costs Profits.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_costs_profits_default_rules_configuration() {
  $rules = array();

  $rule = rules_reaction_rule();

  $rule->label = t('Calculate order profits before order saving.');
  $rule->tags = array('Commerce Order');
  $rule->active = TRUE;
  $rule->weight = 10;

  $rule
    ->event('commerce_order_presave')
    ->condition('commerce_costs_profits_order_profit_needs_calculation', array(
      'commerce_order:select' => 'commerce_order',
    ))
    ->action('commerce_costs_profits_recalculate_order_profits', array(
      'commerce_order:select' => 'commerce_order',
    ));

  $rules['commerce_costs_profits_recalculate_order_profits'] = $rule;

  return $rules;
}
