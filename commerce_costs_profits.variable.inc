<?php

function commerce_costs_profits_group_info() {
  $groups = array();

  $groups['commerce_costs_profits_main_settings'] = array(
    'title' => t('Main costs and profits settings'),
    'access' => 'commerce configure costs products settings',
  );

  return $groups;
}

function commerce_costs_profits_variable_info() {
  $variables = array();

  $variables['commerce_costs_profits_default_margin'] = array(
    'title' => t('Default product margin'),
    'description' => t('If no module provides margin calculation for product and product price recalculation called, this value is used.'),
    'group' => 'commerce_costs_profits_main_settings',
    'type' => 'number',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 2,
      '#maxlength' => 2,
      '#field_suffix' => '%',
    ),
    'default' => 0,
  );
  $variables['commerce_costs_profits_cart_profits_calculation'] = array(
    'title' => t('Calculate order profits for orders with cart statuses'),
    'group' => 'commerce_costs_profits_main_settings',
    'description' => t('Should order profit be calculated when order is in cart and checkout is not completed.'),
    'type' => 'boolean',
    'default' => TRUE,
  );
  $variables['commerce_costs_profits_auto_price_recalculation'] = array(
    'title' => t('Automatically recalculate product price on cost change'),
    'group' => 'commerce_costs_profits_main_settings',
    'description' => t('Should product price be recalculated by this module using margins each time product cost changes.'),
    'type' => 'boolean',
    'default' => FALSE,
  );
  $variables['commerce_costs_profits_expose_expenses_field'] = array(
    'title' => t('Expose "Expenses" field on order edit page'),
    'group' => 'commerce_costs_profits_main_settings',
    'description' => t('If rarely used (it requires hook implemented in custom module) "Expenses" field is to be exposed on order edit page.'),
    'type' => 'boolean',
    'default' => TRUE,
  );
  $variables['commerce_costs_profits_ui_round_precision'] = array(
    'title' => t('Admin area prices round precision'),
    'description' => t('The number of digits to appear after the decimal point; this may be a value between 0 and 20, inclusive.'),
    'group' => 'commerce_costs_profits_main_settings',
    'type' => 'number',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 2,
      '#maxlength' => 2,
    ),
    'default' => 2,
  );

  return $variables;
}
