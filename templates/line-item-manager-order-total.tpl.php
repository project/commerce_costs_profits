<div id='order-edit-order-total'>
  <span id='order-edit-order-total-label'><?php print t('Order total') ?>:</span> 
  <span class='value'><?php print $order_total_string ?></span>
  <div class='recalculation-warning'></div>
</div>