/**
 * @file
 * Provides necessary realtime recalculations reacting on fields editing in line item manager widget.
 */

(function ($) {

  Drupal.behaviors.attach_line_items_profits_form_handlers = {
    attach: function (context, settings) {
      var item_manager = $('#line-item-manager');
      if (item_manager.length === 0) {
        return;
      }

      item_manager.data('precision', settings.costs_total_round_precision);
      var price_control_inputs = item_manager.find('.field-widget-commerce-price-full input, input.field-widget-field-cost, .form-item input[name$="[quantity]"]', context);
      price_control_inputs.on('keyup', {
        currencies: settings.currencies
      }, processPriceValueChanges);
      price_control_inputs.on('keyup', {
        currencies: settings.currencies,
        context: context,
        recalculation_warning: settings.recalculation_warning,
      }, function (event) {
        calculateOrderTotal(event.data.currencies, event.data.context, event.data.recalculation_warning);
      });
      var currency_control_selects = item_manager.find('.field-widget-commerce-price-full select[name$="[currency_code]"]');
      currency_control_selects.on('change', {
        currencies: settings.currencies
      }, processPriceValueChanges);
      currency_control_selects.on('change', {
        currencies: settings.currencies,
        context: context,
        recalculation_warning: settings.recalculation_warning,
      }, function (event) {
        calculateOrderTotal(event.data.currencies, event.data.context, event.data.recalculation_warning);
      });

      // Don't want recalculation to happen on initial page loading
      if ($(context).attr('id') !== undefined) {
        calculateOrderTotal(settings.currencies, context, settings.recalculation_warning);
      }
    }
  };

  function processPriceValueChanges(event) {
    var textfield = $(this);
    var trimmed_value = textfield.val().replace(' ', '');
    if (trimmed_value !== textfield.val()) {
      textfield.val(trimmed_value);
    }

    var row = textfield.parents('#line-item-manager tr');

    var currencyCode = row.find('.field-widget-commerce-price-full select[name$="[currency_code]"] option:selected').val();
    if (currencyCode === undefined) {
      currencyCode = 'default';
    }
    
    var price = row.find('td:nth-child(7) input').val();
    var quantity = row.find('td:nth-child(8) input').val();

    var cost = 0;
    if (row.find('input.field-widget-field-cost').length > 0) {
      cost = row.find('input.field-widget-field-cost').val();
    } else {
      var cost_string = row.children('td:nth-child(4)').html();
      cost = cost_string ? cost_string.replace(/[^0-9]/g, '') : 0;
    }
    var margin = cost > 0 ? Math.round(100 * (price - cost) / cost) : '';
    row.children('td:nth-child(5)').text(margin);
    var precision = $('#line-item-manager').data('precision');
    setFormattedPriceText(row.children('td:nth-child(6)'), price - cost, event.data.currencies[currencyCode], precision);
    setFormattedPriceText(row.children('td:nth-child(9)'), price * quantity, event.data.currencies[currencyCode], precision);
  }

  function calculateOrderTotal(currencies, context, message) {
    var total = 0;
    var precision = $('#line-item-manager').data('precision');
    $('#line-item-manager tbody tr', context).each(function () {
      var row = $(this);
      var quantity = row.find('.form-item input[name$="[quantity]"]').val();
      var price = row.find('.field-widget-commerce-price-full input').val();
      var currencyCode = row.find('.field-widget-commerce-price-full select[name$="[currency_code]"] option:selected').val();
      if (currencyCode === undefined) {
        currencyCode = 'default';
      }
      if (quantity !== undefined && price !== undefined) {
        total += quantity * (price * currencies[currencyCode]['conversion_rate']);
      }
    });
    setFormattedPriceText($('#order-edit-order-total .value', context), total, currencies['default'], precision);
    if (message !== "") {
      $('#order-edit-order-total').find('.recalculation-warning').text(message);
    }
  }

  function setFormattedPriceText(item, price, currency, precision) {
    item.text(currency.symbol_before + price.toFixed(precision) + ' ' + currency.symbol_after);
  }

})(jQuery);
