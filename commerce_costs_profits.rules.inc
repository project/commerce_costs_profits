<?php

/**
 * @file
 * Rules integration for Commerce Costs Profits.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_costs_profits_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_costs_profits_order_profit_needs_calculation'] = array(
    'label' => t('Order profit needs recalculation'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_costs_profits_rules_order_profit_needs_calculation',
    ),
  );

  return $conditions;
}

/**
 * Rules condition: checks to see if the given order profit needs calculation.
 *
 * @param object $order
 * @return bool
 */
function commerce_costs_profits_rules_order_profit_needs_calculation($order) {
  $is_profit_needs_calculation = TRUE;
  if (!variable_get_value('commerce_costs_profits_cart_profits_calculation')) {
    $cart_statuses = array_keys(commerce_order_statuses(array('cart' => TRUE)));
    $is_profit_needs_calculation = !in_array($order->status, $cart_statuses, TRUE);
  }

  return $is_profit_needs_calculation;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_costs_profits_rules_action_info() {
  $actions = array();

  $actions['commerce_costs_profits_recalculate_order_profits'] = array(
    'label' => t('Recalculate order profits.'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce order'),
        'save' => FALSE,
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_costs_profits_recalculate_order_profits_action',
    ),
  );

  return $actions;
}

/**
 * Rules action: Recalculates order profit.
 * Gives other modules a chance to alter order expenses before profit recalculation.
 *
 * @param object $order
 */
function commerce_costs_profits_recalculate_order_profits_action($order) {
  drupal_alter('order_expenses', $order);
  commerce_costs_profits_calculate_order_profit($order);
}

/**
 * @}
 */
